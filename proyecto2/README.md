Visualizador de archivos pdb

¿Que es?
Este programa esta diseñado para entregar información sobre los archivos de Protein Data Bank. Podras ver los primeros 1000 caracteres de el archivo y la imagen de la proteina en cuestión,
ademas, si es que tu lo deseas, podras seleccionar ver el bloque de la proteina que tu quieras y poder guardarlo en tu computador para luego trabajar con el.

¿Como se obtiene?
Para descargar el programa se debe clonar este repositorio y ejecutar python proyecto3.py en Linux

¿Cuales son sus requisitos?
De preferencia se requiere tener Python version 3.8.5 , sistema operativo Linux y tener instalado biopandas, gtk (version sobre 3.0) y pymol.

¿Como se utiliza?

Al iniciar el programa, podras importar el archivo Pdb con el Botón "abrir"
Luego el programa automaticamante mostrará la información relevante del archivo en cuestión y la imagen.
Tendras un menú en donde podras elegir vizualizar distintos bloques de la proteina (“ATOM”, “HETATM”, “ANISOU”, “OTHERS)
y para finalizar el programa te ofrecerá la opción de guardar la información con el boton "guardar"
donde podras elegir en que parte deseas guardar la información, la cual será almacenada en un archivo.txt


Este programa cuenta con  11 funciones las cuales estan diseñadas para importar el archivo, leerlo, vizualizarlo, poder elegir bloque de la proteina y poder guardarlo.
tambien, se utilizó pymol para poder mostrar las imagenes del archivo
Combobox, gtk, y biopandas para crear interfases graficas y mostrar imagenes con su codigo adjunto al enunciado.
a medida que el proyecto iba avanzando, se iba evaluando con flake8 para cumplir con los requisitos del PEP8
Las librerias utilizadas fueron Os, gi, Pymol y biopandas


Cualquier tipo de agradecimiento, duda o comentario, porfavor no duden en contactarnos.

Se despiden cordialmente, 
 
 Martín Castillo
 Lucas Casanova
 Vicente Chandía 

Creadores del codigo.


