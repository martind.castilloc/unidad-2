# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import os 
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from filechooser import FileChooser
from acerca_de import WNinfo
import __main__
# Pymol : quiet and no GUI
__main__.pymol_argv = ["pymol", "- qc"] # noqa : E402
import pymol
pymol.finish_launching()


class wnPrincipal():

    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("./ui/Wnprincipal.ui")
        # Crea ventana principal
        ventana = builder.get_object("WnPrincipal")
        ventana.set_title("Visualizador de proteina")
        ventana.connect("destroy", Gtk.main_quit)
        ventana.resize(800, 600)
        # Imagen
        self.imagen = builder.get_object("imagen")
        # scrolled
        scroll = builder.get_object("Atomo")
        # crear leabel 
        self.nombres = builder.get_object("Nombre")
        self.info = builder.get_object("informacion")
        self.error = builder.get_object("Error")
        # crear botones
        load_btn = builder.get_object("load_btn")
        load_btn.connect("clicked", self.open_filechooser, "open")
        save_btn = builder.get_object("save")
        save_btn.connect("clicked", self.open_filechooser, "save")
        info_btn = builder.get_object("btn_info")
        info_btn.set_label("Acerda de")
        info_btn.connect("clicked", self.on_acept_click)
        # TreeView
        self.informacion = builder.get_object("info")
        self.informacion2 = builder.get_object("info2")
        # lista de opciones
        lista = ["Atom", 
                "Hetatm",
                "Anisou",
                "Others"]
        self.comboboxtext = builder.get_object("opciones")
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
        self.comboboxtext.set_entry_text_column(0)

        for index in lista:
            self.comboboxtext.append_text(index)

        ventana.show_all()

    # seleccionar archivo
    def open_filechooser(self, btn=None, opt=None):
        filechooser = FileChooser(option=opt)
        dialogo = filechooser.dialogo

        # Filtro
        if opt == "open":
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name("PDB Files")
            filter_pdb.add_mime_type("application/x-aportisdoc")
            dialogo.add_filter(filter_pdb)

        response = dialogo.run()

        if response == Gtk.ResponseType.OK:
            self.update_tree(dialogo.get_filename())
            self.ruta = dialogo.get_filename()
            self.nombre_archivo()  
        elif response == Gtk.ResponseType.ACCEPT:
            self.save_pdb_file(dialogo.get_filename())                 
        dialogo.destroy()

    # Abrir el arcivo PDB
    def update_tree(self, pdb_file):
        self.pdb = PandasPdb()
        self.pdb.read_pdb(pdb_file)
        self.info.set_text('\nRaw PDB file contents :\n \n %s \n ...' 
                            % self.pdb.pdb_text[:1000])
                                         
    # Nombre del archivo
    def nombre_archivo(self):
        separar = os.path.split(self.ruta)  
        self.nombre = str(separar[1])
        separar2 = self.nombre.split(".")
        self.name = str(separar2[0])   
        self.nombres.set_text(self.name.upper())
        self.gen_image()  

    # Selecciona la opcion
    def comboboxtext_changed(self, cmb=None):
        self.valor = self.comboboxtext.get_active_text()

        if self.valor == "Atom":
            self.inf_atom()  
        elif self.valor == "Hetatm":
            self.inf_hetatm() 
        elif self.valor == "Anisou":
            self.inf_anisou()
        elif self.valor == "Others":
            self.inf_others()

    # Respuesta a cada opcion
    def inf_atom(self):

        self.inf_atom1 = self.pdb.df["ATOM"]
        self.lrg = len(self.inf_atom1.columns)
        self.modelo = Gtk.ListStore(*(self.lrg*[str]))
        self.informacion.set_model(self.modelo)
        cell = Gtk.CellRendererText()

        for i in range(self.lrg):
            columns = Gtk.TreeViewColumn(self.inf_atom1.columns[i], cell,
                                         text=i)
            self.informacion.append_column(columns)

        for value in self.inf_atom1.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_hetatm(self):
        self.inf_hetatm2 = self.pdb.df['HETATM']
        self.lrg = len(self.inf_hetatm2.columns)
        self.modelo = Gtk.ListStore(*(self.lrg*[str]))
        self.informacion.set_model(self.modelo)

        cell = Gtk.CellRendererText()

        for i in range(self.lrg):
            columns = Gtk.TreeViewColumn(self.inf_hetatm2.columns[i], cell,
                                         text = i)
            self.informacion.append_column(columns)

        for value in self.inf_hetatm2.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_anisou(self):

        self.inf_anisou3 = self.pdb.df['ANISOU']
        self.largo = len(self.inf_anisou3.columns)
        self.modelo = Gtk.ListStore(*(self.largo*[str]))
        self.informacion.set_model(self.modelo)

        cell = Gtk.CellRendererText()

        for i in range(self.largo):
            columns = Gtk.TreeViewColumn(self.inf_anisou3.columns[i], cell,
                                         text = i)
            self.informacion.append_column(columns)

        for value in self.inf_anisou3.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_others(self):

        self.inf_others4 = self.pdb.df['OTHERS']
        self.lrg = len(self.inf_others4.columns)
        self.modelo = Gtk.ListStore(*(self.lrg*[str]))
        self.informacion.set_model(self.modelo)

        cell = Gtk.CellRendererText()

        for i in range(self.lrg):
            columns = Gtk.TreeViewColumn(self.inf_others4.columns[i], cell,
                                         text = i)
            self.informacion.append_column(columns)

        for value in self.inf_others4.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    # funcion del boton informacion
    def on_acept_click(self, btn=None):
        dlg = WNinfo()

        response = dlg.window.run() 

        if response == Gtk.ResponseType.ACCEPT:
            dlg.window.destroy()

    # Guardar el archivo
    def save_pdb_file(self, pdb_file):
        if self.valor == "Atom":
            with open(pdb_file, "w") as newfile:
                self.pdb.to_pdb(path=pdb_file,
                                records=['ATOM'],
                                gz=False,
                                append_newline=True)

        elif self.valor == "Hetatm":
            with open(pdb_file, "w") as newfile:
                self.pdb.to_pdb(path=pdb_file,
                                records=['HETATM'],
                                gz=False,
                                append_newline=True)

        elif self.valor == "Anisou":
            with open(pdb_file, "w") as newfile:
                self.pdb.to_pdb(path=pdb_file,
                                records=['ANISOU'],
                                gz=False,
                                append_newline=True)

        elif self.valor == "Others":
             with open(pdb_file, "w") as newfile:
                self.pdb.to_pdb(path=pdb_file,
                                records=['OTHERS'],
                                gz=False,
                                append_newline=True)

    def gen_image(self):
        name = self.nombre
        aux = self.nombre.split(".")
        pdb_name = aux[0]
        png_file = "".join([self.ruta])
        pdb_file = name
        pymol.cmd.load(self.ruta, pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(pdb_name)
        pymol.cmd.png(png_file)
        pymol.cmd.ray()
        self.imagen.set_from_file(f"{png_file}.png")


if __name__ == "__main__":
    wnPrincipal()
    Gtk.main()
