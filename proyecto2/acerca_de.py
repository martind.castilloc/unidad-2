#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class WNinfo():

    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("./ui/Wnprincipal.ui")

        self.window = builder.get_object("desarrolladores")
        
        self.btn_accept = builder.get_object("aceptar")

        self.window.show_all()
