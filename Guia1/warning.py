import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class warning():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./objetos.ui")

        self.warning = builder.get_object("warning")

        self.aceptar = builder.get_object("aceptar2")
        self.cancelar = builder.get_object("cancelar")

        self.warning.show_all()
