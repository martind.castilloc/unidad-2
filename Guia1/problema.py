#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from info import alerta
from warning import warning


class Mainwindow():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./objetos.ui")

        ventana = builder.get_object("principal")
        ventana.connect("destroy", Gtk.main_quit)
        ventana.resize(800, 600)

        # boton aceptar
        acept = builder.get_object("aceptar")
        acept.connect("clicked", self.on_acept_click)

        # boton reset
        reset = builder.get_object("reset")
        reset.connect("clicked", self.on_reset_click)
        reset.set_label("reset")

        # cuadro de texto con su respectivo label
        self.texto = builder.get_object("texto1")
        self.label = builder.get_object("lbl")
        self.texto2 = builder.get_object("texto2")
        self.label2 = builder.get_object("lbl2")

        self.respuesta = builder.get_object("respuesta")

        # Mostrar la ventana principal
        ventana.show_all()

        # Accion del boton aceptar
    def on_acept_click(self, btn=None):
        dlg = alerta()
        texto = self.texto.get_text()
        texto2 = self.texto2.get_text()
        dlg.informacion.format_secondary_text(f"{texto}  y  {texto2}")
        response = dlg.informacion.run()

        if response == Gtk.ResponseType.ACCEPT:
            dlg.informacion.destroy()

        suma = 0
        suma2 = 0
        cd = texto.isdigit()
        cd2 = texto2.isdigit()

        if cd and cd2 == True:
            for i in texto:
                suma = suma + int(i)
            for c in texto2:
                suma2 = suma2 + int(c)
            smt = suma + suma2
            cnv = str(smt)
            self.respuesta.set_text(f"la suma es {cnv}")

        else:
            lg = len(texto)
            lg2 = len(texto2)
            suma = lg + lg2
            tfm = str(suma)

            self.respuesta.set_text(f"la suma de sus cadenas es {tfm}")

        # Accion del boton reset
    def on_reset_click(self, btn=None):
        advertencia = warning()
        response = advertencia.warning.run()

        if response == Gtk.ResponseType.ACCEPT:
            self.texto.set_text("")
            self.texto2.set_text("")
            self.respuesta.set_text("Respuesta")
            advertencia.warning.destroy()

        elif response == Gtk.ResponseType.CANCEL:
            advertencia.warning.destroy()


if __name__ == "__main__":
    Mainwindow()
    Gtk.main()
