#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pandas as pd 
import matplotlib.pyplot as plt
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from ventana2 import WNMensaje


def get_dataframe():

    x = "suministro.csv"
    data = pd.read_csv(x, sep = ",")
    data = data.fillna(0)
    return data

class problemas():
  
    def __init__(self):
       
        # Contruccion Ventana Principal
        builder = Gtk.Builder()
        builder.add_from_file("./ventanap.ui")

        ventana = builder.get_object("Ventana_P")
        ventana.connect("destroy", Gtk.main_quit)
        ventana.resize(800, 600)

        # boton No funciono como deberia
        #info = builder.get_object("btn")
        #info.connect("clicked", self.on_acept_click)

        # Labels
        self.label = builder.get_object("lbl")
        self.label2 = builder.get_object("lbl2")

        self.respuestas = builder.get_object("respuesta1")
        self.respuesta2 = builder.get_object("pregunta2")

        # lista de preguntas
        lista = ["¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?", 
        "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?",
        "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?",
        "¿Que pais consume mas carne?",
        "¿Que pais tiene una mayor tasa de muerte a causa del covid-19?"]

        self.comboboxtext = builder.get_object("list_preguntas")
        
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
       
        self.comboboxtext.set_entry_text_column(0)

        for index in lista:
            self.comboboxtext.append_text(index)

        self.dataframe = get_dataframe() 
        
        ventana.show_all()
"""
    def on_acept_click(self, btn=None):
        dlg = WNMensaje()

        response = dlg.mensaje.run()

        if response == Gtk.ResponseType.ACCEPT:
            dlg.mensaje.destroy()

"""
    def comboboxtext_changed(self, cbt=None):
        valor = self.comboboxtext.get_active_text()
        
        if valor == "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?":
            pass
                      
        if valor == "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?":
            pass
            
        if valor == "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?":

            alcohol = self.dataframe[["Country","Alcoholic Beverages"]] 
            self.ordenar = alcohol.sort_values(by="Alcoholic Beverages", ascending=False)
            self.entrega = self.ordenar.head(10)

            confirmados = self.dataframe[["Country","Confirmed"]] 
            self.ord = confirmados.sort_values(by="Confirmed", ascending=False)
            self.entre = self.ord.head(10)

            #Entrega de Alcohol
            self.forma = Gtk.ListStore(*(2*[str]))
            self.respuestas.set_model(model=self.forma) 

            cell = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(title="Paises",
                                    cell_renderer=cell,
                                    text=0)
            self.respuestas.append_column(column)

            column2 = Gtk.TreeViewColumn(title="Alcohol",
                                    cell_renderer=cell,
                                    text=1)
            self.respuestas.append_column(column2)

            self.forma.append(["Country", "Alcoholic Beverages"])
            for i in self.entre.values:
                Datos = [str(x) for x in i ]
                self.forma.append(Datos) 

            #Entrega Confirmados
            self.forma2 = Gtk.ListStore(*(2*[str]))
            self.respuesta2.set_model(model=self.forma2) 

            cell2 = Gtk.CellRendererText()
            columna = Gtk.TreeViewColumn(title="Paises",
                                    cell_renderer=cell2,
                                    text=0)
            self.respuesta2.append_column(columna)

            columnb = Gtk.TreeViewColumn(title="Confirmados",
                                    cell_renderer=cell2,
                                    text=1)
            self.respuesta2.append_column(columnb)

            self.forma2.append(["Country", "Confirmados"])
            for i2 in self.entrega.values:
                Datos2 = [str(x) for x in i2]
                self.forma2.append(Datos2) 

            
        if valor == "¿Que pais consume mas carne?":

            df1 = pd.DataFrame(self.dataframe)
            alto1 = df1.loc[2, "Deaths"]
            
            for i in range(2, len(df1)):

                if df1.loc[i, "Deaths"] > df1.loc[i-1, "Deaths"] and df1.loc[i, "Deaths"] > alto1:
                    alto1 = df1.loc[i, "Deaths"]
                    country1 = df1.loc[i, "Country"]
            self.label.set_text(f"El pais que consume mas carne es {country1} con una tasa de {alto1}%")

        if valor == "¿Que pais tiene una mayor tasa de muerte a causa del covid-19?":
            df = pd.DataFrame(self.dataframe)
            alto = df.loc[2, "Deaths"]
            
            for i in range(2, len(df)):

                if df.loc[i, "Deaths"] > df.loc[i-1, "Deaths"] and df.loc[i, "Deaths"] > alto:
                    alto = df.loc[i, "Deaths"]
                    country = df.loc[i, "Country"]

            self.label2.set_text(f"El pais con la mayor tasa de muerte a causa del covid-19 es {country} con una tasa de {alto}%")

        
    
if __name__ == "__main__":
    problemas()
    Gtk.main()